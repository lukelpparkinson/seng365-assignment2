import Vue from 'vue';
import App from './App';
import Venues from './Venues';
import Login from "./Login";
import Register from "./Register"
import AddVenue from "./AddVenue";
import AddReview from "./AddReview"
import MyProfile from "./MyProfile";
import AllProfiles from "./AllProfiles";
import './assets/styles.css';
import 'vuetify/dist/vuetify.min.css';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import Vuetify from 'vuetify';
import VeeValidate from 'vee-validate';
import VueCookies from 'vue-cookies';
import OtherProfile from "./OtherProfile";


Vue.use(VueResource);
Vue.use(VueRouter);
Vue.use(Vuetify);
Vue.use(VeeValidate);
Vue.use(VueCookies);



Vue.http.options.emulateJSON = true;

const routes = [
  {
    path: "/",
    component: Venues
  },
  {
    path: "/login/register",
    name: "Register",
    component: Register
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/venues",
    name: "Venues",
    component: Venues
  },
  {
    path: "/venues/city/:city",
    name: "VenuesCity",
    component: Venues
  },
  {
    path: "/venues/add",
    name: "VenuesAdd",
    component: AddVenue
  },
  {
    path: "/venues/add/review/:venueId",
    name: "AddReview",
    component: AddReview
  },
  {
    path: "/profiles/me",
    name: "MyProfile",
    component: MyProfile
  },
  {
    path: "/profiles",
    name: "AllProfiles",
    component: AllProfiles
  },
  {
    path: "/profiles/find/:id",
    name: "OtherProfile",
    component: OtherProfile
  }
];

const router = new VueRouter({
  routes: routes,
  mode: 'history'
});

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
});
